# Xavki project

## 1. Objectifs

Ce repo est dédié au montage d'une infra devops et à la création de pipeline de déploiement.

Merci à la chaine youtbe de Xavki pour tout son travail sur les technologies devops

## 2. Useful links

* [Lien vers la chaine de Xavki](https://www.youtube.com/c/xavki-linux/playlists)
* [Lien vers le tuto](https://www.youtube.com/watch?v=tiSfXCM8VTw&list=PLn6POgpklwWrBPMKFniOiMyLMdxlgFhrG)
* [Lien vers le repo](https://gitlab.com/xavki/pipeline-saison-1)

## [3. Infrastructure](docs/Infrastructure.md)

## [4. Installation des outils devops](docs/Installation.md)

## [5. Applicatif](docs/Applicatif.md)

## [6. Ansible](docs/Ansible.md)

## [7. Jmeter](docs/Jmeter.md)
