# Jmeter

## Description

Jmeter est un binaire que nous alons télécharger. Il dispose d'une interface graphique pour la préparation de nos tests, les tests seront lancés grâce à un CLI dédié.
<br/>Un plugin Jenkins est disponible, ainsi que de nombreuses fonctionnalités.

## Utilisation dans notre contexte

Dans ce contexte nous allons utiliser Jmeter afin de lancer les tests suivants:
* Simuler une utilisation
* amont: connaitre ses utlisateurs / charge cible
* test de charge: lectures et écritures
* tests fonctionnels: lectures et écritures
* peu poussés (prise en compte des risques, caractères spéciaux)
* check:
  * retour : code HTTP + data
  * base de données
* TESTS : personnalisation maximum
* Important : monitoring (CPU, RAM, espace disque)

[Documentation officielle](https://jmeter.apache.org/)

## Définition

* Thread Group : définitions des utilisateurs (nombre de users, nombre de requête...)
* Sampler : type de requête
* Listener: valorisation des résultats (graphiques, tableaux)
* Logic Controller : coordination des samplers
* Assertions : contrôle des résultats 
* Timers : gestion des temps de pause
* Config Element : additif de configuration des samplers (csv, cookies, data)
* Post Processor : agir avec la réponse d'un sapler (regex)
* Pre Processor : action avant les samplers 

## Tests

* GET http://mon-app:8080/questions
* GET htt://mon-app:8080/questions?sort=createdAt.desc
* `"Content-Type: application/json" -X POST -d '{"title":"mon titre","description":"ma description"}' http://mon-app:8080/questions`
* ̀`"Content-Type: application/json" -X POST -d '{"text":"voici ma réponse"}' http://mon-app:8080/questions/1000/answers`