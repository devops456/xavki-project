# Installation

## Objectif

Description des différentes étapes d'installation

## 1. Prérequis

Installer Vagrant [lien vers la documentation officcielle](https://www.vagrantup.com/), vous y trouverez tous les éléments pour bien démarrer (téléchargement, getting-started...).
Vagrant nous permet ici d'initialiser nos VM qui serviront de support à l'[infrastructure](./Infrastructure.md).

Ici nous utiliserons Virtual-Box comme hyperviseur [lien vers le site officiel](https://www.virtualbox.org/) 

## 2. Commandes vagrant utiles

* Initier un dossier : `vagrant init`
* Monter les serveurs définis dans le Vagrantfile : `vagrant up`
* Se connecter au serveur : `vagrant ssh <mon-serveur>`

## 3. Jenkins

Pour se connecter à Jenkins : http://192.168.56.10:8080

## 4. La registry

Pour se connecter à la registry `http://192.168.56.15` avec:
* user: xavki
* password: password

*Le user/mdp sont configurés dans le script d'installation avec la commande suivante : `docker run --entrypoint htpasswd registry:2.7.0 -Bbn xavki password > passwd/htpasswd`, pour personnaliser, modifier remplacer simplement avec votre user/mdp dans la commande*

## 5. Gitlab

Pour se connecter:
* Soit directement avec l'ip : http://192.168.56.16 (peut causer des problème lors des push et pull, je déconseille)
* Soit en passant par le DNS : http://gitlab.example.com/, Dans ce cas il faut modifier le fichier hosts:
  * Sous linux:
    * en tant que root : `echo -e "192.168.56.16\tgitlab.example.com" >> /etc/hosts`
    * en tant que sudoer : `echo -e "192.168.56.16\tgitlab.example.com" | sudo tee -a /etc/hosts`
  * Sous windows: dans le repertoire : `C:\Windows\System32\drivers\etc`, avec notepadd (ou n'importe quel editeur de text) ouvrez le fichier avec les droits administrateur `hosts` et ajouter la ligne suivante:
    * `192.168.56.16    gitlab.example.com`

**TO DO** : workflow gitlab

<br/>[**Retour Sommaire**](../README.md)