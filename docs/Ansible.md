# Ansible

## 1. Description

Ansible est un outil développé en python qui permet de déployer/installer à distance via SSH.

Il permet :
* installation d'infrastructure as code (IAAS)
* déploiement type pipline

## 2. Installation

Sous ubuntu-18 : `$ sudo apt install ansible`

## 3. Définitions

* Inventory : liste des serveurs et variables
* Task/tâche : une action à réaliser
* Modules : fonctions qui sont appelées par des tâches. [Documentation officielle des modules](https://docs.ansible.com/ansible/2.8/modules/list_of_all_modules.html)
* Rôle : regroupement de tâches visant à déployer/installer un bloc spécifique cohérent
* Playbook: Définition des rôles devant être joués sur quels groupes de serveurs (inventory)
* Groupes : division regroupant des serveurs par catégorie au sein de l'inventory
* Variables

## 4. Inventaire

Le fichier [`inventory.yml`](../ansible/inventory.yml) permet de regourper les serveurs cibles

```yaml
all:
  children:
    jenkins:
      vars:
        my_var: "Jenkins var"
      hosts:
        192.168.56.10:
    registry:
      vars:
        my_var: "Registry var"
      hosts:
        192.168.56.15:
```

## 5. Utilisation de modules de base

### 5.1 Ping

Dans cet exemple nous allons ping les machine listé dans le fichier `inventory.yml`, en utilisant le mot de passe pour se connecter. il est d'usage de procéder à un échange de clé ssh, mais pour l'exemple le mot de passe suffira.
[Lien vers la documentation du module ping](https://docs.ansible.com/ansible/2.8/modules/ping_module.html#ping-module)

* Installer sshpass : `$ sudo apt install sshpass`
* Lancer le module via le CLI Ansible:

```bash
$ ansible -i inventory.yml all -u vagrant -k -m ping
SSH password: 
192.168.56.15 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
192.168.56.10 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
```
**Les paramètres :**
* `-i` : spécifie le chemin vers le fichier `inventory.yml`
* `-u` : spécifie un user pour la connexion
* `-k` : permet l'utilisation de mot de passe
* `-m` : spécifie un module

### 5.2 Shell

Dans cet exemple nous allons exécuter du shell au sein des serveurs de l'inventaire. [Lien vers la documentation du module shell](https://docs.ansible.com/ansible/2.8/modules/shell_module.html#shell-module)

```bash
$ ansible -i inventory.yml all -u vagrant -k -m shell -a "hostname"
SSH password: 
192.168.56.15 | SUCCESS | rc=0 >>
registry-pipeline

192.168.56.10 | SUCCESS | rc=0 >>
p1jenkins-pipeline
```
**Les paramètres :**
* `-a` : Permet de préciser les arguments à passer au module shell, pour plus d'info : `$ansible -m shell --help`

## 6. Utilisation des variables

### 6.1 Directement dans l'inventaire (déconseillé)

Pour passer une variable, il faut utiliser la synthaxe Jinja ([la doc Jinja](https://jinja.palletsprojects.com/en/2.11.x/)):

```bash
$ ansible -i inventory.yml all -u vagrant -k -m shell -a "echo '{{my_var}}'"
SSH password: 
192.168.56.15 | SUCCESS | rc=0 >>
Registry var

192.168.56.10 | SUCCESS | rc=0 >>
Jenkins var
```
### 6.2 Dans les dossiers `group_vars` et `host_vars`

Les `group_vars` et les `host_vars`, sont des répertoires créés dans le workspace (ici [le dossier **ansible** à la racine](../ansible))

Ci-dessous l'arborescence :

```bash
.
├── group_vars
│   ├── all.yml
│   └── registry.yml
├── host_vars
│   ├── 192.168.56.10.yml
│   └── 192.168.56.15.yml
└── inventory.yml
```

Il est préférable de déclarer les variables globales dans le fichier [`group_vars/all.yml`](../ansible/group_vars/all.yml).

On peut également faire correspondre un fichier avec la déclaration du groupe dans le fichier inventory. Comme c'est le cas ici avec le groupe `registry.yml` et le fichier de variables associées `groups_vars/registry.yml`

*P.s: Ce sont les variables groups qui surchargent les variables globales (celles du all.yml)*

## 7. Les playbooks

Un playbooks Ansible est un fichier YAML dans lesquels sont mentionnés toutes les tâches qu'Ansible doit exécuter. La syntaxe quasi-naturelle utilisée pour décrire ces tâches est particulièrement simple. Le fichier YAML commence par décrire les hôtes (ou serveurs) ciblés puis les variables du processus avant de dérouler les jobs à accomplir.

Exemple de playbook très basique :

```yml
- name: "mon playbook"
  hosts: jenkins
  tasks:
    - name: tasks 1
      shell: echo "Hello world"
```

Pour lancer ce playblook : 
```bash
$ ansible-playbook -i inventory.yml -u vagrant -k playbook.yml 
SSH password: 

PLAY [mon playbook] ******************************************************************************************

TASK [Gathering Facts] ******************************************************************************************
ok: [192.168.56.10]

TASK [tasks 1] ******************************************************************************************
changed: [192.168.56.10]

PLAY RECAP ******************************************************************************************
192.168.56.10              : ok=2    changed=1    unreachable=0    failed=
```

## 8. Les rôles

les rôles définis par Ansible sont un ensemble de tâches qui s’assurent de la présence/absence d’une fonctionnalité spécifique (allant d’un utilisateur linux à un cluster Kubernetes d’une centaine de nœuds).

### 8.1 Créer des roles

Pour créer une arborescence de rôles: ` $ ansible-galaxy init mon_role`. Cela va créer les repertoires/fichiers suivants :

```bash
.
├── group_vars
│   ├── all.yml
│   └── registry.yml
├── host_vars
│   ├── 192.168.56.10.yml
│   └── 192.168.56.15.yml
├── inventory.yml
├── playbook-roles.yml
├── playbook.yml
└── roles
    └── mon_role
        ├── defaults
        │   └── main.yml
        ├── files
        ├── handlers
        │   └── main.yml
        ├── meta
        │   └── main.yml
        ├── README.md
        ├── tasks
        │   └── main.yml
        ├── templates
        ├── tests
        │   ├── inventory
        │   └── test.yml
        └── vars
            └── main.yml
```

* `tasks/main.yml` : point d'entrée de notre rôle
* `defaults/main.yml` : variables par défaut qui ont vocation à être surchargée
* `files` :fichier non modifié
* `handlers` : trigger appeler selon le contexte
* `templates` : modèle de fichier avec des variables dedans
* `tests`: pour testerle rôle
* `vars` : variables fixe

### 8.2 Lancer le role

#### 8.2.1 Modifier le playbook
Créer un nouveau playbook `playbook-roles.yml`:
```yml
- name: "playbook with roles"
  hosts: jenkins
  roles: 
  - mon_role
```

*P.s : Ici un nouveau playbook a été créé pour l'exemple, vous pouvez simplement modifier le fichier `playbook.yml` à la place*

#### 8.2.2 Lancer le playbook

```bash
$ ansible-playbook -i inventory.yml -u vagrant -k playbook-roles.yml
SSH password: 

PLAY [playbook with roles] ******************************************************************************************

TASK [Gathering Facts] ******************************************************************************************
ok: [192.168.56.10]

TASK [mon_role : task 1] ******************************************************************************************
changed: [192.168.56.10]

PLAY RECAP ******************************************************************************************
192.168.56.10              : ok=2    changed=1    unreachable=0    failed=0
```

### 8.3 Vérification sur le serveur 

```bash
vagrant@p1jenkins-pipeline:~$ ssh vagrant@192.168.56.10

vagrant@p1jenkins-pipeline:~$ cat /tmp/testfile 
Hello world... Again
```

## 9. Les fichiers et templates

Pour la suite des opérations, nous allons procéder à deux changements:
* Echange de clefs SSH entre le serveur master (celui sur lequel est installé Ansible) et les noeuds :
  * resgitry: 192.168.56.15
  * jenkins :192.168.56.10
* Modification du fichier `/etc/hosts` pour privilégier l'utilisation des noms de domaine et non les adresses IPs

**Echange de clef :** 
* `$ ssh-copy-id vagrant@192.168.56.10`
* `$ ssh-copy-id vagrant@192.168.56.16`

**Modification du /etc/hosts**:
* Ouvrir le fichier hosts avec votre éditeur de texte préféré: `$ sudo vim /etc/hosts`
* Ajouter les lignes suivantes (les noms de domaines sont personalisables) : 
  * `192.168.56.16	gitlab.example.com`
  * `192.168.56.10	jenkins.example.com`
  * `192.168.56.15	my-registry.com`

Ainsi on modifie le fichier `inventory.yml`:
```yml
all:
  children:
    jenkins:
      hosts:
        jenkins.example.com:
    registry:
      hosts:
        my-registry.com:
```

### 9.1 Envoie d'un fichier

* Créer un fichier contenant du texte : `$ echo "Hello from file" > ansible/roles/mon_role/files/monfichier.txt` 
* Editer le fichier : `$ vim ansible/roles/mon_roles/tasks/main.yml`:
```yml
  ---
- name: task 1
  copy:
    src: monfichier.txt
    dest: /tmp/fichier.txt
    owner: vagrant
    mode: 0755
  ```
* Si ce n'est pas déjà fait, éditer le playbook pour qu'il éxécute le role *mon_role* (cf [8.2.1 Modifier le playbook](####-8.2.1-Modifier-le-playbook))
* Lancer le playbook:
```bash
$ ansible-playbook -i inventory.yml -u vagrant playbook-roles.yml 

PLAY [playbook with roles] ******************************************************************************************

TASK [Gathering Facts] ******************************************************************************************
ok: [jenkins.example.com]

TASK [mon_role : task 1] ******************************************************************************************
changed: [jenkins.example.com]

PLAY RECAP ******************************************************************************************
jenkins.example.com        : ok=2    changed=1    unreachable=0    failed=0  
```

*P.s: À noter que maintenant que les clés ssh sont échangées, nous n'utilisons plus le paramètre `-k`*

### 9.2 Gestion des templates

**Préparation**

Un template est un fichier modèle qui est surchargé par des variables. Ici nous allons envoyer un template sur les deux noeuds, il contiendra deux variables:
* Une variable globale (déclarée dans le [`ansible/group_vars/all.yml`](../ansible/group_vars/all.yml))
* Une variable spécifique au noeud:
  * Pour le noeud registry [`ansible/host_vars/my-registry.com.yml`](../ansible/host_vars/my-registry.com.yml)
  * Pour le noeud jenkins [`ansible/host_vars/jenkins.example.com.yml`](../ansible/host_vars/jenkins.example.com.yml)

**Création du template**

Créez et éditez un fichier template : `$ vim ansible/roles/mon_role/templates/monfichier.txt.j2` (j2 : extension jinja (à titre indicatif))
```jinja
This is a global variable : {{ my_global_var }}
This is a local varible :
    hostname : {{ hostname }}
```
**Lancer le role**
```bash
 $ ansible-playbook -i inventory.yml -u vagrant playbook-roles.yml 

PLAY [playbook with roles] ******************************************************************************************

TASK [Gathering Facts] ******************************************************************************************
ok: [my-registry.com]
ok: [jenkins.example.com]

TASK [mon_role : copy file] ******************************************************************************************
changed: [my-registry.com]
changed: [jenkins.example.com]

TASK [mon_role : copy template] ******************************************************************************************
changed: [my-registry.com]
changed: [jenkins.example.com]

PLAY RECAP ******************************************************************************************
jenkins.example.com        : ok=3    changed=2    unreachable=0    failed=0   
my-registry.com            : ok=3    changed=2    unreachable=0    failed=0   

```

**Vérification**
* Noeud jenkins:
```
vagrant@p1jenkins-pipeline:~$ cat /tmp/fichier.txt 
This is a global variable : my_global_value
This is a local varible :
    hostname : jenkins
```
* Noeud registry:
```
vagrant@registry-pipeline$ cat /tmp/fichier.txt
This is a global variable : my_global_value
This is a local varible :
    hostname : registry
```

## 10. Les Handlers

## 11. Importer des roles avec ansible-galaxy

requirment.yml:

```yml
- src: https://github.com/ultransible/docker.git
  name: docker
  version: master
  scm: git
```

Import du role: `$ ansible-galaxy install --roles-path roles/ -r requirement.yml`

## 12. Utiliser ansible-galaxy pour importer des roles

1. Placez-vous dans a la racine du projet ansible (en amont des roles/)
2. Editer le fichier `requirement.yml` 
```yml
- src: http://gitlab.example.com/mypipeline/install-docker.git
  name: docker
  version: master
  scm: git

- src: http://gitlab.example.com/mypipeline/service-install-run.git
  name: service_install_run
  version: master
  scm: git

- src: http://gitlab.example.com/mypipeline/service-postgres.git
  name: postgres
  version: master
  scm: git
```
Dans notre cas nous allons importer les roles contenu dans chacun des repo recuperes en local.
[Lien vers les repos](https://gitlab.com/xavki/pipeline-saison-1/-/tree/master/1.46-ansible-split)

3. Lancer la commande : `ansible-galaxy install --roles-path roles/ -r requirements.yml`
```bash
> $ ansible-galaxy install --roles-path roles/ -r requirements.yml 
- extracting docker to /home/mchevrel/workspace/01_Gitlab/01_DevOps/local_gitlab/deploy-ansible/roles/docker
- docker (master) was installed successfully
- extracting service_install_run to /home/mchevrel/workspace/01_Gitlab/01_DevOps/local_gitlab/deploy-ansible/roles/service_install_run
- service_install_run (master) was installed successfully
- extracting postgres to /home/mchevrel/workspace/01_Gitlab/01_DevOps/local_gitlab/deploy-ansible/roles/postgres
- postgres (master) was installed successfully
```

4. Verification :
```bash
> $ tree -L 1 roles
roles
├── docker
├── postgres
└── service_install_run
```

<br/>[**Retour Sommaire**](../README.md)
