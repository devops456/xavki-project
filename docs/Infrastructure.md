# Infrastructure

## Schéma d'architecture

```mermaid
flowchart TB
  subgraph Environnement
  srv_dev[[srv_dev]]
  srv_stage[[srv_stage]]
  srv_prod[[srv_prod]]
  end
  subgraph Jenkins
  end
  Jenkins-->|orchestre|Environnement
  DB[(PostgreSQL)]
  
  Jenkins-->|orchestre|DB
  subgraph Gitlab
  end
  Jenkins-->|orchestre|Gitlab
  Registry --> |Provide|Environnement
  Jenkins-->|orchestre|Registry
  DB-->|Data|Environnement
  Environnement-->|Data|DB
```

## Serveurs

Les serveurs de l'infrastructure seront montés à l'aide de **Vagrant**

### Serveur Jenkins

* IP: 192.168.56.10/24
* hosntame: p1jenkins-pipeline
* Hardware:
  * 2 CPUs
  * 3 Go RAM
* Java: default-jre
* Jenkins
* ansible / sshpass / gpg
* docker: pour le build et pour préparer les environnement
* connexion ssh 
  * user : vagrant
  * passwd : vagrant
* Autres:
  * ansible: pipeline true et allow readable tmp true
  * docker: usermod -aG docker Jenkins
  * registry: insecure registry (pour éviter de gérer les certificats)

### Serveurs applicatifs

3 serveurs:
* srv-dev:
  * hostnatme: srvdev-pipeline
  * IP: 192.168.56.11/24
* srv-stage:
  * hostnatme: srvstage-pipeline
  * IP: 192.168.56.12/24
* srv-dev:
  * hostnatme: srvprod-pipeline
  * IP: 192.168.56.13/24

Même config pour les 3 environnements:
* Hardware:
  * 1 CPU
  * 512 Mo RAM
* connexion ssh 
  * user : vagrant
  * passwd : vagrant

### Serveur Base de données

* IP: 192.168.56.14/24
* hosntame: srvbdd-pipeline
* Hardware:
  * 1 CPU
  * 512 Mo RAM
* PostgreSQL
* connexion ssh et PostgreSQL
  * user : vagrant
  * passwd: vagrant
* 3 BDD: dev/stage/prod
* Configuration:
  * `/etc/.../postgresql.conf : listen all "*"`
  * `/etc/.../pg_hba.conf` open for all ~~`127.0.0.1/32`~~ -> `0.0.0.0` (pour notre test à ne pas faire sinon)

### Serveur de registry

* IP: 192.168.56.15/24
* hosntame: registry-pipeline
* Hardware:
  * 1 CPU
  * 512 Mo RAM
* docker-compose
* certificat ssl
* generation de password (docker exec)

<br/>[**Retour Sommaire**](../README.md)