# Objectif

Cette partie est dédié à la partie applicative du déploiement

## Maven

### Vous-avez dit maven ?

La présentation de maven est très bien décrite dans cette [documentation](http://www-igm.univ-mlv.fr/~dr/XPOSE2003/site_maven/).
Voici un extrait des première lignes

> #### A quoi sert Maven ?
>
> Maven est un outil permettant d'automatiser la gestion de projets Java.
> Il offre entre autres les fonctionalités suivantes :
>
> * Compilation et déploiement des applications Java (JAR, WAR)
> * Gestion des librairies requises par l'application
> * Exécution des tests unitaires
> * Génération des documentations du projet (site web, pdf, Latex)
> * Intégration dans différents IDE (Eclipse, JBulder)

### Liens utiles

* [Lien vers la documentation officielle](https://maven.apache.org/guides/index.html)
* [Le fichier pom.xml](https://gitlab.com/devops456/my_app/-/blob/master/pom.xml) de notre application. Ce fichier permet à maven de gérer les dépendances
* [Un dépôt maven](https://mvnrepository.com/), regroupant les différentes librairies est disponible en ligne


### Key words

* **validate :** validate the project is correct and all necessary information is avaible
* **compile :** compile the source code of the project
* **test :** test the compiled code and package it ins distribuatble format, such as a JAR
* **verfify :** run any checks on results of integration tests to ensure quality criteria are met
* **install :** install the package into the local repository, for use as a dependency in other projects locally
* **deploy :** done in the build environment, copies the final package to the remote repository for sharing wwith other developers and projects

### Utilisation locale

:warning: Comme Jenkins utilise Java, il est déconseillé d'installer maven sur la machine herbegeant Jenkins (possible conflit de version de java)

Une meilleure pratique consiste à instancier une image docker contenant maven qui gérera Java de manière indépendante.

<br/>[**Retour Sommaire**](../README.md)