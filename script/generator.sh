#!/bin/bash


###############################
#
# Genrateur de docker-compose
#
###############################

## Variables ##

WORKSPACE="${HOME}/workspace"
DIR="${WORKSPACE}/postgres"
USER_SCRIPT=${USER}

## Fonctions ##

help(){

	echo "Usage:
	${0##*/} [-h] [--help]
  
	Options:
		-h, --help: aide

		-p, --postgres : lance une instance postgres
	
		-i, --ip : affichage des ip

		-d, --down : détruit les éléments déclaré dans le compose (conteneur, reseau et volume)
	"

}

postgres(){
	psql_user="postgres"
	psql_passwd="postgres"
	echo ""
	echo "Instanciation d'une image postgres..."
	echo ""
	echo "Création du repertoire de data"
	[ -d "$DIR " ] && echo "$DIR existe déjà" || mkdir -p $DIR/postgres
	echo ""
	echo "
version: '3.7'
services:
  postgres:
    image: postgres:latest
    container_name: postgres
    environment:
    - POSTGRES_USER=${psql_user}
    - POSTGRES_PASSWORD=${psql_passwd}
    - POSTGRES_DB=mydb
    ports:
    - 5432:5432
    volumes:
    - postgres_data:/var/lib/postgres
    networks:
    - generator
volumes:
  postgres_data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${DIR}/postgres
networks:
  generator:
    driver: bridge
    ipam:
      config:
        - subnet: 192.169.10.0/24

" > $DIR/docker-compose-postgres.yml
	echo ""
	echo "Run de l'instance"
	docker-compose -f ${DIR}/docker-compose-postgres.yml up -d
	echo ""
	echo "Credential:
  - Port : 5432
	- POSTGRES_USER: ${psql_user}
  - POSTGRES_PASSWORD: ${psql_passwd}
  - POSTGRES_DATABASE: mydb
Command : psql -h <ip> -U ${psql_user} -d mydb"
}

ip(){
	echo "==================== infos conteneurs ===================="
	for container_id in $(docker ps -q); do
		echo "Container ID : ${container_id}"
		docker inspect -f "Container IP: {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}" $container_id
		docker inspect -f "Container Name: {{.Name}}" $container_id
		echo "=========================================================="
	done

}

down(){
	for folder in $(ls ${WORKSPACE}); do
		if [ -e $WORKSPACE/$folder/*.yml ]; then
			compose_file=$(ls $WORKSPACE/$folder/*.yml)
			echo $compose_file
			docker-compose -f $compose_file down
		fi
	done
}

parser_option(){

case $@ in 
	-h|--help)
		help
	;;
	
	-p|--postgres)
		postgres
	;;	
	-i|--ip)
		ip
	;;
	-d|--down)
		down
	;;
	*)
		echo "option invalide"
esac
}

## Main ##

parser_option $@
ip
