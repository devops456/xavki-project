#!/bin/bash

# Command a lancer pour installer postgresql

apt update
apt install sudo
apt install -y wget vim
apt install gnupg
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" >> /etc/apt/sources.list.d/pgdg.list
apt update
apt install -y postgresql-11
