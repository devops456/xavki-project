node {
    cleanWs()
    try {
        stage('my_first_stage'){
            sh "echo 'Hello world !'"
        }
        stage('my_snd_stage'){
            echo "Hello again !"
        }
    }
    finally{
        cleanWs()
    }
}